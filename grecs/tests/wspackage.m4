m4_define([AT_PACKAGE_NAME],      [wordsplit])
m4_define([AT_PACKAGE_TARNAME],   [wordsplit])
m4_define([AT_PACKAGE_VERSION],   [v1.1])
m4_define([AT_PACKAGE_STRING],    [AT_PACKAGE_TARNAME AT_PACKAGE_VERSION])
m4_define([AT_PACKAGE_BUGREPORT], [bug-dico@gnu.org])
